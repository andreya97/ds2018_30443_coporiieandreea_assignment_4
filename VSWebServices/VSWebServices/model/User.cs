﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VSWebServices.model
{
    public class User
    {
        private int id;
        private string username;
        private string password;
        private bool admin;

        public User() {}

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public User(string username, string password, bool admin)
        {
            this.username = username;
            this.password = password;
            this.admin = admin;
        }
        
        public int Id { get; set; }
        public string Password { get { return this.password; } set { this.password = value; } }
        public string Username { get { return this.username; } set { this.username = value; } }
        public bool Admin { get { return this.admin; } set { this.admin = value; } }
    }
}
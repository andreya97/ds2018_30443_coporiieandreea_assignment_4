﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VSWebServices.model
{
    public class Pack
    {
        private int id;
        private int id_client1;
        private int id_client2;
        private string name;
        private string description;
        private string from_city;
        private string to_city;
        private bool tracking;
        
        public Pack() {}
        
        public Pack(int id_client1, int id_client2, string name, string description, string from_city, string to_city)
        {
            this.id_client1 = id_client1;
            this.id_client2 = id_client2;
            this.name = name;
            this.description = description;
            this.from_city = from_city;
            this.to_city = to_city;
        }

        public Pack(int id_client1, int id_client2, string name, string description, string from_city, string to_city, bool tracking)
        {
            this.id_client1 = id_client1;
            this.id_client2 = id_client2;
            this.name = name;
            this.description = description;
            this.from_city = from_city;
            this.to_city = to_city;
            this.tracking = tracking;
        }

        public int Id { get; set; }
        public int Id_client1 { get; set; }
        public int Id_client2 { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string From_city { get; set; }
        public string To_city { get; set; }
        public bool Tracking { get; set; }
    }
}
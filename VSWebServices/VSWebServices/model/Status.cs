﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VSWebServices.model
{
    public class Status
    {
        private int id;
        private string city;
        private DateTime time;
        private int id_pack;

        public Status() {}

        public Status(string city, DateTime time, int id_pack)
        {
            this.city = city;
            this.time = time;
            this.id_pack = id_pack;
        }

        public int Id { get; set; }
        public string City { get; set; }
        public DateTime Time { get; set; }
        public int Id_pack { get; set; }
        
    }
}
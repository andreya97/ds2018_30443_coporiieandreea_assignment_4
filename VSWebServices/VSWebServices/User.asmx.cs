﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VSWebServices.dao;
using VSWebServices.model;

namespace VSWebServices
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        PackDAO packDAO = new PackDAO();
        UserDAO userDAO = new UserDAO();
        StatusDAO statusDAO = new StatusDAO();

        [WebMethod]
        public bool register(string username, string password)
        {
            if (userDAO.existent(username)) return false;
            userDAO.register(new User(username, password));
            return true;
        }

        [WebMethod]
        public User login(string username, string password)
        {
            return userDAO.login(username, password);
        }

        [WebMethod]
        public List<Pack> getAllPacks()
        {
            return packDAO.getAll();
        }

        [WebMethod]
        public List<Pack> getAllPacksForUser(int user_id)
        {
            return packDAO.getAllForUser(user_id);
        }

        [WebMethod]
        public Pack findPack(string name)
        {
            return packDAO.findByName(name);
        }

        [WebMethod]
        public Status checkPackageStatus(int pack_id)
        {
            return statusDAO.get(pack_id);
        }

      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using VSWebServices.model;

namespace VSWebServices.dao
{
    public class PackDAO
    {
        private const string CONNECTION_STRING = "server=localhost;user=root;database=assign-four-db;port=3306;password=mysql";
        private MySqlConnection connection = new MySqlConnection(CONNECTION_STRING);

        public List<Pack> getAll()
        {
            string query = "SELECT * from Pack";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader result = cmd.ExecuteReader();
            List<Pack> packs = new List<Pack>();
            while (result.Read())
            {
                Pack pack = new Pack();
                pack.Id = (int)result["id"];
                pack.Id_client1 = (int)result["id_client1"];
                pack.Id_client2 = (int)result["id_client2"];
                pack.Name = (string)result["name"];
                pack.Description = (string)result["description"];
                pack.From_city = (string)result["from_city"];
                pack.To_city = (string)result["to_city"];
                pack.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                packs.Add(pack);
            }
            connection.Close();
            result.Close();
            if (packs != null)
                return packs;
            else
                return null;
        }

        public List<Pack> getAllForUser(int id)
        {
            string query = "SELECT * from Pack WHERE id_client1 = @id1 OR id_client2 = @id2";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@id1", id);
            cmd.Parameters.AddWithValue("@id2", id);
            MySqlDataReader result = cmd.ExecuteReader();
            List<Pack> packs = new List<Pack>();
            while (result.Read())
            {
                Pack pack = new Pack();
                pack.Id = (int)result["id"];
                pack.Id_client1 = (int)result["id_client1"];
                pack.Id_client2 = (int)result["id_client2"];
                pack.Name = (string)result["name"];
                pack.Description = (string)result["description"];
                pack.From_city = (string)result["from_city"];
                pack.To_city = (string)result["to_city"];
                pack.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                packs.Add(pack);
            }
            connection.Close();
            result.Close();
            if (packs != null)
                return packs;
            else
                return null;
        }

        public Pack findByName(string name)
        {
            string query = "SELECT * FROM Pack WHERE name=@name";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@name", name);
            MySqlDataReader result = cmd.ExecuteReader();
            if (result.Read())
            {
                Pack pack = new Pack();
                pack.Id = (int)result["id"];
                pack.Id_client1 = (int)result["id_client1"];
                pack.Id_client2 = (int)result["id_client2"];
                pack.Name = (string)result["name"];
                pack.Description = (string)result["description"];
                pack.From_city = (string)result["from_city"];
                pack.To_city = (string)result["to_city"];
                pack.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                connection.Close();
                result.Close();
                return pack;
            }
            connection.Close();
            result.Close();
            return null;
        }

        public Pack findById(int id)
        {
            string query = "SELECT * FROM package WHERE id=@id";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader result = cmd.ExecuteReader();
            if (result.Read())
            {
                Pack pack = new Pack();
                pack.Id = (int)result["id"];
                pack.Id_client1 = (int)result["id_client1"];
                pack.Id_client2 = (int)result["id_client2"];
                pack.Name = (string)result["name"];
                pack.Description = (string)result["description"];
                pack.From_city = (string)result["from_city"];
                pack.To_city = (string)result["to_city"];
                pack.Tracking = Convert.ToInt32(result["tracking"]) != 0;
                connection.Close();
                result.Close();
                return pack;
            }
            connection.Close();
            result.Close();

            return null;
        }
    }
}
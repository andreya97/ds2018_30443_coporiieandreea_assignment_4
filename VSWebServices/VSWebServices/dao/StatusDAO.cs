﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using VSWebServices.model;

namespace VSWebServices.dao
{
    public class StatusDAO
    {
        private const string CONNECTION_STRING = "server=localhost;user=root;database=assign-four-db;port=3306;password=mysql";
        private MySqlConnection connection = new MySqlConnection(CONNECTION_STRING);
        
        public Status get(int pack_id)
        {
            string query = "SELECT * FROM status WHERE id_pack=@id ORDER BY time DESC";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@id", pack_id);
            MySqlDataReader result = cmd.ExecuteReader();
            List<Status> route = new List<Status>();
            while (result.Read())
            {
                Status status = new Status();
                status.Id = (int)result["id"];
                status.City = (string)result["city"];
                status.Time = (DateTime)result["time"];
                status.Id_pack = pack_id;
                route.Add(status);
            }
            connection.Close();
            result.Close();
            if (!route.Any())
            {
                return null;
            }
            return route[0];
        }
    }
}
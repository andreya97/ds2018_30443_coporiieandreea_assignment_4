﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using VSWebServices.model;
 
namespace VSWebServices.dao
{
    public class UserDAO
    {
        private const string CONNECTION_STRING = "server=localhost;user=root;database=assign-four-db;port=3306;password=mysql";
        private MySqlConnection connection = new MySqlConnection(CONNECTION_STRING);

      
       

        public bool register(User user)
        {
            string query = "INSERT INTO user (username,password,admin) VALUES (@username,@password,@admin)";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@username", user.Username);
            cmd.Parameters.AddWithValue("@password", user.Password);
            cmd.Parameters.AddWithValue("@admin", false);

            int rowsAffected = cmd.ExecuteNonQuery();
            connection.Close();
            if (rowsAffected == 0)
            {
                return false;
            }
            return true;
        }

        public User login(string username, string password)
        {
            string query = "SELECT * from User WHERE username = @username AND password= @password";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            MySqlDataReader result = cmd.ExecuteReader();
            if (result.Read())
            {
                
                User user = new User();
                user.Id = (int)result["id"];
                user.Username = username;
                user.Password = password;
                user.Admin = Convert.ToInt32(result["admin"]) != 0;
                connection.Close();
                return user;
            }
            connection.Close();
            return null;
        }

        public bool existent(string username)
        {
            string query = "SELECT * FROM user WHERE username=@username";
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@username", username);
            MySqlDataReader result = cmd.ExecuteReader();
            
            if (result.Read())
            {
                connection.Close();
                result.Close();
                return true;
            }
            connection.Close();
            result.Close();
            return false;
        }

    }
}
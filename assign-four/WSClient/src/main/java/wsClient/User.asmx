<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:element name="register">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="registerResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="registerResult" type="s:boolean" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="login">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="username" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="loginResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="loginResult" type="tns:User" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="User">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Password" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Username" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Admin" type="s:boolean" />
        </s:sequence>
      </s:complexType>
      <s:element name="getAllPacksForUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="user_id" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAllPacksForUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getAllPacksForUserResult" type="tns:ArrayOfPack" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfPack">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="Pack" nillable="true" type="tns:Pack" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Pack">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Id_client1" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="Id_client2" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="From_city" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="To_city" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Tracking" type="s:boolean" />
        </s:sequence>
      </s:complexType>
      <s:element name="findPack">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="name" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="findPackResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="findPackResult" type="tns:Pack" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="checkPackageStatus">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="pack_id" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="checkPackageStatusResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="checkPackageStatusResult" type="tns:Status" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="Status">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="City" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Time" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="Id_pack" type="s:int" />
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="registerSoapIn">
    <wsdl:part name="parameters" element="tns:register" />
  </wsdl:message>
  <wsdl:message name="registerSoapOut">
    <wsdl:part name="parameters" element="tns:registerResponse" />
  </wsdl:message>
  <wsdl:message name="loginSoapIn">
    <wsdl:part name="parameters" element="tns:login" />
  </wsdl:message>
  <wsdl:message name="loginSoapOut">
    <wsdl:part name="parameters" element="tns:loginResponse" />
  </wsdl:message>
  <wsdl:message name="getAllPacksForUserSoapIn">
    <wsdl:part name="parameters" element="tns:getAllPacksForUser" />
  </wsdl:message>
  <wsdl:message name="getAllPacksForUserSoapOut">
    <wsdl:part name="parameters" element="tns:getAllPacksForUserResponse" />
  </wsdl:message>
  <wsdl:message name="findPackSoapIn">
    <wsdl:part name="parameters" element="tns:findPack" />
  </wsdl:message>
  <wsdl:message name="findPackSoapOut">
    <wsdl:part name="parameters" element="tns:findPackResponse" />
  </wsdl:message>
  <wsdl:message name="checkPackageStatusSoapIn">
    <wsdl:part name="parameters" element="tns:checkPackageStatus" />
  </wsdl:message>
  <wsdl:message name="checkPackageStatusSoapOut">
    <wsdl:part name="parameters" element="tns:checkPackageStatusResponse" />
  </wsdl:message>
  <wsdl:portType name="WebService1Soap">
    <wsdl:operation name="register">
      <wsdl:input message="tns:registerSoapIn" />
      <wsdl:output message="tns:registerSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="login">
      <wsdl:input message="tns:loginSoapIn" />
      <wsdl:output message="tns:loginSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getAllPacksForUser">
      <wsdl:input message="tns:getAllPacksForUserSoapIn" />
      <wsdl:output message="tns:getAllPacksForUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="findPack">
      <wsdl:input message="tns:findPackSoapIn" />
      <wsdl:output message="tns:findPackSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <wsdl:input message="tns:checkPackageStatusSoapIn" />
      <wsdl:output message="tns:checkPackageStatusSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="WebService1Soap" type="tns:WebService1Soap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="register">
      <soap:operation soapAction="http://tempuri.org/register" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="login">
      <soap:operation soapAction="http://tempuri.org/login" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAllPacksForUser">
      <soap:operation soapAction="http://tempuri.org/getAllPacksForUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="findPack">
      <soap:operation soapAction="http://tempuri.org/findPack" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <soap:operation soapAction="http://tempuri.org/checkPackageStatus" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="WebService1Soap12" type="tns:WebService1Soap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="register">
      <soap12:operation soapAction="http://tempuri.org/register" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="login">
      <soap12:operation soapAction="http://tempuri.org/login" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAllPacksForUser">
      <soap12:operation soapAction="http://tempuri.org/getAllPacksForUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="findPack">
      <soap12:operation soapAction="http://tempuri.org/findPack" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="checkPackageStatus">
      <soap12:operation soapAction="http://tempuri.org/checkPackageStatus" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="WebService1">
    <wsdl:port name="WebService1Soap" binding="tns:WebService1Soap">
      <soap:address location="http://localhost:51692/User.asmx" />
    </wsdl:port>
    <wsdl:port name="WebService1Soap12" binding="tns:WebService1Soap12">
      <soap12:address location="http://localhost:51692/User.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>
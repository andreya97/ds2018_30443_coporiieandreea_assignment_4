
package wsClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getAllPacksForUserResult" type="{http://tempuri.org/}ArrayOfPack" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllPacksForUserResult"
})
@XmlRootElement(name = "getAllPacksForUserResponse")
public class GetAllPacksForUserResponse {

    protected ArrayOfPack getAllPacksForUserResult;

    /**
     * Gets the value of the getAllPacksForUserResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPack }
     *     
     */
    public ArrayOfPack getGetAllPacksForUserResult() {
        return getAllPacksForUserResult;
    }

    /**
     * Sets the value of the getAllPacksForUserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPack }
     *     
     */
    public void setGetAllPacksForUserResult(ArrayOfPack value) {
        this.getAllPacksForUserResult = value;
    }

}

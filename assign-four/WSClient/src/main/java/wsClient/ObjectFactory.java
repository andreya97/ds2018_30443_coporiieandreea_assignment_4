
package wsClient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindPack }
     * 
     */
    public FindPack createFindPack() {
        return new FindPack();
    }

    /**
     * Create an instance of {@link GetAllPacks }
     * 
     */
    public GetAllPacks createGetAllPacks() {
        return new GetAllPacks();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link GetAllPacksResponse }
     * 
     */
    public GetAllPacksResponse createGetAllPacksResponse() {
        return new GetAllPacksResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPack }
     * 
     */
    public ArrayOfPack createArrayOfPack() {
        return new ArrayOfPack();
    }

    /**
     * Create an instance of {@link FindPackResponse }
     * 
     */
    public FindPackResponse createFindPackResponse() {
        return new FindPackResponse();
    }

    /**
     * Create an instance of {@link Pack }
     * 
     */
    public Pack createPack() {
        return new Pack();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link GetAllPacksForUser }
     * 
     */
    public GetAllPacksForUser createGetAllPacksForUser() {
        return new GetAllPacksForUser();
    }

    /**
     * Create an instance of {@link GetAllPacksForUserResponse }
     * 
     */
    public GetAllPacksForUserResponse createGetAllPacksForUserResponse() {
        return new GetAllPacksForUserResponse();
    }

    /**
     * Create an instance of {@link CheckPackageStatus }
     * 
     */
    public CheckPackageStatus createCheckPackageStatus() {
        return new CheckPackageStatus();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link CheckPackageStatusResponse }
     * 
     */
    public CheckPackageStatusResponse createCheckPackageStatusResponse() {
        return new CheckPackageStatusResponse();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

}

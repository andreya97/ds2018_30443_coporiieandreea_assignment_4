
package wsClient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="checkPackageStatusResult" type="{http://tempuri.org/}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkPackageStatusResult"
})
@XmlRootElement(name = "checkPackageStatusResponse")
public class CheckPackageStatusResponse {

    protected Status checkPackageStatusResult;

    /**
     * Gets the value of the checkPackageStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getCheckPackageStatusResult() {
        return checkPackageStatusResult;
    }

    /**
     * Sets the value of the checkPackageStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setCheckPackageStatusResult(Status value) {
        this.checkPackageStatusResult = value;
    }

}

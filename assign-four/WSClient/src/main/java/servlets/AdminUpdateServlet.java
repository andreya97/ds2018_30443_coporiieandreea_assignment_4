package servlets;

import wsAdmin.AdminService;
import wsAdmin.AdminServiceService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminUpdateServlet extends HttpServlet {

    AdminService wsAdmin = new AdminServiceService().getAdminServicePort();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = 0;
        try {
            String city = req.getParameter("currentCity");
            id = Integer.parseInt(req.getParameter("packUpdate"));
            wsAdmin.updatePackStatus(id, city);
        } catch (Exception e) {
            resp.sendRedirect("../../Error.html");
        }
        resp.sendRedirect("../admin/route?packId=" + id);
    }
}

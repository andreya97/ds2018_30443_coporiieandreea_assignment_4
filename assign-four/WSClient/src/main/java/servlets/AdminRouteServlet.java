package servlets;

import util.HtmlUtil;
import wsAdmin.AdminService;
import wsAdmin.AdminServiceService;
import wsAdmin.Status;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdminRouteServlet extends HttpServlet {

    AdminService wsAdmin = new AdminServiceService().getAdminServicePort();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        int id = Integer.parseInt(req.getParameter("packId"));
        List<Status> route = wsAdmin.findRoute(id);

        HtmlUtil.includeHeader(out);
        out.println("<h2>Current route of the pack:</h2></br>");
        HtmlUtil.getRouteTable(out, route, id);
    }
}

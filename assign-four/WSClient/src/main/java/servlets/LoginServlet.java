package servlets;

import wsClient.User;
import wsClient.WebService1;
import wsClient.WebService1Soap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {

    WebService1Soap wsClient = new WebService1().getWebService1Soap();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
          req.getRequestDispatcher("/Login.html").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        User u = wsClient.login(req.getParameter("username"),req.getParameter("password"));
        if (u == null) {
            resp.sendRedirect("/Error.html");
        } else {
            session.setAttribute("USER",u);
            if (u.isAdmin()) {
                resp.sendRedirect("/admin");
            } else {
                resp.sendRedirect("/user");
            }
        }
    }
}

package servlets;

import wsAdmin.AdminService;
import wsAdmin.AdminServiceService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminDeleteServlet extends HttpServlet {

    AdminService wsAdmin = new AdminServiceService().getAdminServicePort();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            int no = Integer.parseInt(req.getParameter("packDel"));
            wsAdmin.deletePack(no);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            resp.sendRedirect("../Error.html");
            return;
        }
       resp.sendRedirect("../admin");
    }
}

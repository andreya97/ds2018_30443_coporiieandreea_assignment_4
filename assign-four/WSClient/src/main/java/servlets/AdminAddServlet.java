package servlets;

import wsAdmin.AdminService;
import wsAdmin.AdminServiceService;
import wsAdmin.Pack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminAddServlet extends HttpServlet {

    AdminService wsAdmin = new AdminServiceService().getAdminServicePort();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String name = req.getParameter("name");
            String description = req.getParameter("description");
            String fromCity = req.getParameter("fromCity");
            String toCity = req.getParameter("toCity");

            int idcl1 = Integer.parseInt(req.getParameter("idClient1"));
            int idcl2 = Integer.parseInt(req.getParameter("idClient2"));
            if (fromCity.equals(toCity) || idcl1 == idcl2) throw new Exception();

             Pack p = new Pack();
             p.setName(name);
             p.setDescription(description);
             p.setIdClient1(idcl1);
             p.setIdClient2(idcl2);
             p.setFromCity(fromCity);
             p.setToCity(toCity);
             p.setTracking(false);
             wsAdmin.addPack(p);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            resp.sendRedirect("../Error.html");
            return;
        }
        resp.sendRedirect("../admin");
    }
}

package servlets;

import util.HtmlUtil;
import wsClient.Status;
import wsClient.WebService1;
import wsClient.WebService1Soap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class UserStatusServlet extends HttpServlet {

    WebService1Soap wsClient = new WebService1().getWebService1Soap();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        HtmlUtil.includeHeader(out);
        out.println("<h2>Current status:</h2></br>");
        int no = Integer.parseInt(req.getParameter("packId"));
        Status sts = wsClient.checkPackageStatus(no);
        out.println("<h2>Placed in " + sts.getCity() + " at "
                + sts.getTime().toString().split("T")[0] + " "
                + sts.getTime().toString().split("T")[1] +
                "</h2></br>");
    }
}

package servlets;

import util.HtmlUtil;
import wsClient.Pack;
import wsClient.User;
import wsClient.WebService1;
import wsClient.WebService1Soap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UserServlet extends HttpServlet {

    WebService1Soap wsClient = new WebService1().getWebService1Soap();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User u = (User)req.getSession().getAttribute("USER");

        PrintWriter out = resp.getWriter();

        HtmlUtil.includeHeader(out);
        out.println("<body>");
        out.println("<h1>Welcome, " + u.getUsername()  + "!</h1></br>");
        out.println("<h2>Looking for a particular package?</h2>");
        out.println("</br>");
        out.println("<form class=\"form-inline\" action=\"/user/search\" method=\"GET\">");
        out.println("<input type=\"text\" name=\"packName\" class=\"form-control input-sm\"  placeholder=\"Package name\">");
        out.println("<button type=\"submit\" class=\"btn btn-sm btn-primary\">Submit</button>");
        out.println("</form>");


        out.println("</br>");

        String searchedPack = (String)req.getAttribute("search");
        if (searchedPack == null) {
            HtmlUtil.getPacksTable(out, u, wsClient.getAllPacksForUser(u.getId()).getPack());
        } else {
            List<Pack> packlist= new ArrayList<>();
            Pack p = wsClient.findPack(searchedPack);
            if (p != null) packlist.add(p);
            HtmlUtil.getPacksTable(out, u, packlist);
        }

        out.println();
        out.println("</body>");
    }

}

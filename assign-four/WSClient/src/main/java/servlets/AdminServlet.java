package servlets;

import util.HtmlUtil;
import wsClient.User;
import wsClient.WebService1;
import wsClient.WebService1Soap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AdminServlet extends HttpServlet {

    WebService1Soap wsClient = new WebService1().getWebService1Soap();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User u = (User)req.getSession().getAttribute("USER");

        PrintWriter out = resp.getWriter();

        HtmlUtil.includeHeader(out);
        out.println("<body>");
        out.println("<h1>Welcome, " + u.getUsername()  + "!</h1><br/>");

        HtmlUtil.getPacksTable(out, u, wsClient.getAllPacks().getPack());

        //HtmlUtil.includeAddForm(out, cityDAO);

        out.println("</body>");
    }

}
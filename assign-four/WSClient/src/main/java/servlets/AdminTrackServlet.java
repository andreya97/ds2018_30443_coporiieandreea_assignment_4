package servlets;

import wsAdmin.AdminService;
import wsAdmin.AdminServiceService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminTrackServlet extends HttpServlet {

    AdminService wsAdmin = new AdminServiceService().getAdminServicePort();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("packTrack"));
        if (!wsAdmin.registerPackForTracking(id)) {
            resp.sendRedirect("../Error.html");
        }
        resp.sendRedirect("../admin");
    }
}

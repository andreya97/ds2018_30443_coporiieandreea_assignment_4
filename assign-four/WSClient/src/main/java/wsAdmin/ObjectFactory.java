
package wsAdmin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsAdmin package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RegisterPackForTracking_QNAME = new QName("http://admin/", "registerPackForTracking");
    private final static QName _AddPack_QNAME = new QName("http://admin/", "addPack");
    private final static QName _UpdatePackStatusResponse_QNAME = new QName("http://admin/", "updatePackStatusResponse");
    private final static QName _AddPackResponse_QNAME = new QName("http://admin/", "addPackResponse");
    private final static QName _FindRoute_QNAME = new QName("http://admin/", "findRoute");
    private final static QName _UpdatePackStatus_QNAME = new QName("http://admin/", "updatePackStatus");
    private final static QName _FindRouteResponse_QNAME = new QName("http://admin/", "findRouteResponse");
    private final static QName _RegisterPackForTrackingResponse_QNAME = new QName("http://admin/", "registerPackForTrackingResponse");
    private final static QName _DeletePack_QNAME = new QName("http://admin/", "deletePack");
    private final static QName _DeletePackResponse_QNAME = new QName("http://admin/", "deletePackResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsAdmin
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddPack }
     * 
     */
    public AddPack createAddPack() {
        return new AddPack();
    }

    /**
     * Create an instance of {@link RegisterPackForTracking }
     * 
     */
    public RegisterPackForTracking createRegisterPackForTracking() {
        return new RegisterPackForTracking();
    }

    /**
     * Create an instance of {@link UpdatePackStatusResponse }
     * 
     */
    public UpdatePackStatusResponse createUpdatePackStatusResponse() {
        return new UpdatePackStatusResponse();
    }

    /**
     * Create an instance of {@link FindRouteResponse }
     * 
     */
    public FindRouteResponse createFindRouteResponse() {
        return new FindRouteResponse();
    }

    /**
     * Create an instance of {@link AddPackResponse }
     * 
     */
    public AddPackResponse createAddPackResponse() {
        return new AddPackResponse();
    }

    /**
     * Create an instance of {@link FindRoute }
     * 
     */
    public FindRoute createFindRoute() {
        return new FindRoute();
    }

    /**
     * Create an instance of {@link UpdatePackStatus }
     * 
     */
    public UpdatePackStatus createUpdatePackStatus() {
        return new UpdatePackStatus();
    }

    /**
     * Create an instance of {@link RegisterPackForTrackingResponse }
     * 
     */
    public RegisterPackForTrackingResponse createRegisterPackForTrackingResponse() {
        return new RegisterPackForTrackingResponse();
    }

    /**
     * Create an instance of {@link DeletePack }
     * 
     */
    public DeletePack createDeletePack() {
        return new DeletePack();
    }

    /**
     * Create an instance of {@link DeletePackResponse }
     * 
     */
    public DeletePackResponse createDeletePackResponse() {
        return new DeletePackResponse();
    }

    /**
     * Create an instance of {@link LocalDateTime }
     * 
     */
    public LocalDateTime createLocalDateTime() {
        return new LocalDateTime();
    }

    /**
     * Create an instance of {@link Pack }
     * 
     */
    public Pack createPack() {
        return new Pack();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackForTracking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "registerPackForTracking")
    public JAXBElement<RegisterPackForTracking> createRegisterPackForTracking(RegisterPackForTracking value) {
        return new JAXBElement<RegisterPackForTracking>(_RegisterPackForTracking_QNAME, RegisterPackForTracking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "addPack")
    public JAXBElement<AddPack> createAddPack(AddPack value) {
        return new JAXBElement<AddPack>(_AddPack_QNAME, AddPack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "updatePackStatusResponse")
    public JAXBElement<UpdatePackStatusResponse> createUpdatePackStatusResponse(UpdatePackStatusResponse value) {
        return new JAXBElement<UpdatePackStatusResponse>(_UpdatePackStatusResponse_QNAME, UpdatePackStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "addPackResponse")
    public JAXBElement<AddPackResponse> createAddPackResponse(AddPackResponse value) {
        return new JAXBElement<AddPackResponse>(_AddPackResponse_QNAME, AddPackResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "findRoute")
    public JAXBElement<FindRoute> createFindRoute(FindRoute value) {
        return new JAXBElement<FindRoute>(_FindRoute_QNAME, FindRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "updatePackStatus")
    public JAXBElement<UpdatePackStatus> createUpdatePackStatus(UpdatePackStatus value) {
        return new JAXBElement<UpdatePackStatus>(_UpdatePackStatus_QNAME, UpdatePackStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRouteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "findRouteResponse")
    public JAXBElement<FindRouteResponse> createFindRouteResponse(FindRouteResponse value) {
        return new JAXBElement<FindRouteResponse>(_FindRouteResponse_QNAME, FindRouteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackForTrackingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "registerPackForTrackingResponse")
    public JAXBElement<RegisterPackForTrackingResponse> createRegisterPackForTrackingResponse(RegisterPackForTrackingResponse value) {
        return new JAXBElement<RegisterPackForTrackingResponse>(_RegisterPackForTrackingResponse_QNAME, RegisterPackForTrackingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "deletePack")
    public JAXBElement<DeletePack> createDeletePack(DeletePack value) {
        return new JAXBElement<DeletePack>(_DeletePack_QNAME, DeletePack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin/", name = "deletePackResponse")
    public JAXBElement<DeletePackResponse> createDeletePackResponse(DeletePackResponse value) {
        return new JAXBElement<DeletePackResponse>(_DeletePackResponse_QNAME, DeletePackResponse.class, null, value);
    }

}


package wsAdmin;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "AdminServiceService", targetNamespace = "http://admin/", wsdlLocation = "http://localhost:9000/admin?wsdl")
public class AdminServiceService
    extends Service
{

    private final static URL ADMINSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException ADMINSERVICESERVICE_EXCEPTION;
    private final static QName ADMINSERVICESERVICE_QNAME = new QName("http://admin/", "AdminServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:9000/admin?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ADMINSERVICESERVICE_WSDL_LOCATION = url;
        ADMINSERVICESERVICE_EXCEPTION = e;
    }

    public AdminServiceService() {
        super(__getWsdlLocation(), ADMINSERVICESERVICE_QNAME);
    }

    public AdminServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), ADMINSERVICESERVICE_QNAME, features);
    }

    public AdminServiceService(URL wsdlLocation) {
        super(wsdlLocation, ADMINSERVICESERVICE_QNAME);
    }

    public AdminServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ADMINSERVICESERVICE_QNAME, features);
    }

    public AdminServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public AdminServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns AdminService
     */
    @WebEndpoint(name = "AdminServicePort")
    public AdminService getAdminServicePort() {
        return super.getPort(new QName("http://admin/", "AdminServicePort"), AdminService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns AdminService
     */
    @WebEndpoint(name = "AdminServicePort")
    public AdminService getAdminServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://admin/", "AdminServicePort"), AdminService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ADMINSERVICESERVICE_EXCEPTION!= null) {
            throw ADMINSERVICESERVICE_EXCEPTION;
        }
        return ADMINSERVICESERVICE_WSDL_LOCATION;
    }

}

package util;

import wsAdmin.Status;
import wsClient.Pack;
import wsClient.User;
import wsClient.WebService1;
import wsClient.WebService1Soap;

import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.List;

public class HtmlUtil {

    static WebService1Soap wsClient = new WebService1().getWebService1Soap();


    public static void includeHeader(PrintWriter out) {
        out.println("<head>");
        out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">");
        out.println("</head>");
    }

    public static void getPacksTable(PrintWriter out, User u, List<Pack> packs) {
        String statusAction = u.isAdmin() ? "admin/route" : "user/status";

        out.println("<table class=\"table\" style=\"border-spacing: 85px 3px; position: relative; bottom: 0px\" align=\"center\">");
        out.println("<tr>");
        // out.println("<th>Pack Id</th>");
        out.println("<th>Name</th>");
        out.println("<th>Description</th>");
        out.println("<th>From Client</th>");
        out.println("<th>To Client</th>");
        out.println("<th>From City</th>");
        out.println("<th>To City</th>");
        out.println("<th>Tracking</th>");
        if (u.isAdmin()) out.println("<th></th>"); //start tracking
        out.println("<th>View " + statusAction.split("/")[1] + "</th>"); // for the status/route buttons
        if (u.isAdmin()) {
            out.println("<th></th>"); // ofr delete buttons }
        }
        out.println("</tr>");

        if (packs.isEmpty() || packs == null) {
            out.println("<h2>No packs found.");
        } else
            for (Pack p : packs) {
                out.println("<tr>");
                // out.println("<td>" + p.getId() + "</td>");
                out.println("<td>" + p.getName() + "</td>");
                out.println("<td>" + p.getDescription() + "</td>");
                out.println("<td>" + (p.getIdClient1() == u.getId() ? "you" : p.getIdClient1()) + "</td>");
                out.println("<td>" + (p.getIdClient2() == u.getId() ? "you" : p.getIdClient2()) + "</td>");
                out.println("<td>" + p.getFromCity() + "</td>");
                out.println("<td>" + p.getToCity() + "</td>");
                out.println("<td>" + p.isTracking() + "</td>");
                if (u.isAdmin()) {
                    out.println("<td>");
                    out.println("<form action=\"/admin/track\" method=\"POST\">");
                    out.println("<input type=\"hidden\" name=\"packTrack\" value=\"" + p.getId() + "\">");
                    out.println("<button type=\"submit\" class=\"btn btn-sm btn-secondary\"" + (p.isTracking() == true ? "disabled" : "") + ">Start Tracking</button>");
                    out.println("</form>");
                    out.println("</td>");
                }
                out.println("<td>");
                out.println("<form action=\"" + statusAction + "\" method=\"GET\">");
                out.println("<input type=\"hidden\" name=\"packId\" value=\"" + p.getId() + "\">");
                out.println("<button type=\"submit\" class=\"btn btn-sm btn-primary \"" + (p.isTracking() == false ? "disabled" : "") + ">" +
                        statusAction.split("/")[1] +
                        "</button>");
                out.println("</form>");
                out.println("</td>");
                if (u.isAdmin()) {
                    out.println("<td>");
                    out.println("<form action=\"/admin/delete\" method=\"POST\">");
                    out.println("<input type=\"hidden\" name=\"packDel\" value=\"" + p.getId() + "\">");
                    out.println("<button type=\"submit\" class=\"btn btn-sm btn-danger\">X</button>");
                    out.println("</form>");
                    out.println("</td>");
                }
                out.println("</tr>");
            }
        if (u.isAdmin()) {
            includeAddForm(out);
        }
        out.println("</table>");
    }

    public static void includeAddForm(PrintWriter out) {
        out.println("<tr>");
        out.println("<form action=\"/admin/add\" method=\"POST\">");
     //   style="width: 50px;"
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"name\"></td>");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"description\"></td>");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"idClient1\"></td>");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"idClient2\"></td>");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"fromCity\"></td>");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"toCity\"></td>");
        out.println("<td></td>");
        out.println("<td></td>");
        out.println("<td><button type=\"submit\" class=\"btn btn-sm\"> &#9745;</button></td>");
        out.println("</form>");
        out.println("</tr>");
    }

    public static void getRouteTable(PrintWriter out, List<Status> route, int packid) {
        out.println("<table class=\"table\" style=\"border-spacing: 85px 3px; position: relative; bottom: 0px\" align=\"center\">");
        out.println("<tr>");
        out.println("<th>City</th>");
        out.println("<th>Time</th>");
        out.println("<th></th>");
        out.println("</tr>");
        for (Status s : route) {
            out.println("<tr>");
            out.println("<td>" + s.getCity() + "</td>");
            out.println("<td><p>" + s.getTime().toString().split("T")[0] + " "
                    + s.getTime().toString().split("T")[1] +"</p></td>");
            out.println("</tr>");
        }
        out.println("<tr>");
        out.println("<form action=\"/admin/update\" method=\"POST\">");
        out.println("<td><input class=\"form-control\" type=\"text\" name=\"currentCity\"></td>");
        out.println("<input type=\"hidden\" name=\"packUpdate\" value=\"" + packid + "\">");
        out.println("<td><button type=\"submit\" class=\"btn btn-sm\"> &#9745;</button></td>");
        out.println("</form>");
        out.println("</tr>");
        out.println("</table>");
    }

}

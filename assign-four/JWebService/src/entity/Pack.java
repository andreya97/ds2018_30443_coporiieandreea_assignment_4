package entity;

public class Pack {

    int id;
    int id_client1;
    int id_client2;
    String name;
    String description;
    String from_city;
    String to_city;
    boolean tracking;

    public Pack() {}

    public Pack(int id_client1, int id_client2, String name, String description, String from_city, String to_city) {
        this.id_client1 = id_client1;
        this.id_client2 = id_client2;
        this.name = name;
        this.description = description;
        this.from_city = from_city;
        this.to_city = to_city;
    }

    public Pack(int id_client1, int id_client2, String name, String description, String from_city, String to_city, boolean tracking) {
        this.id_client1 = id_client1;
        this.id_client2 = id_client2;
        this.name = name;
        this.description = description;
        this.from_city = from_city;
        this.to_city = to_city;
        this.tracking = tracking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_client1() {
        return id_client1;
    }

    public void setId_client1(int id_client1) {
        this.id_client1 = id_client1;
    }

    public int getId_client2() {
        return id_client2;
    }

    public void setId_client2(int id_client2) {
        this.id_client2 = id_client2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrom_city() {
        return from_city;
    }

    public void setFrom_city(String from_city) {
        this.from_city = from_city;
    }

    public String getTo_city() {
        return to_city;
    }

    public void setTo_city(String to_city) {
        this.to_city = to_city;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }
}

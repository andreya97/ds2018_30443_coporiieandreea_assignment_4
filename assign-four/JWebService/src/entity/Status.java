package entity;

import java.time.LocalDateTime;

public class Status {

    int id;
    String city;
    LocalDateTime time;
    int id_pack;

    public Status(){}

    public Status(String city, LocalDateTime time, int id_pack) {
        this.city = city;
        this.time = time;
        this.id_pack = id_pack;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getId_pack() {
        return id_pack;
    }

    public void setId_pack(int id_pack) {
        this.id_pack = id_pack;
    }
}

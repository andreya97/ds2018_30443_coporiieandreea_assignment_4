package admin;

import dao.PackDAO;
import dao.StatusDAO;
import entity.Pack;
import entity.Status;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.time.LocalDateTime;
import java.util.List;

@WebService()
public class AdminService {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9000/admin",
                new AdminService());
    }

    private PackDAO packDAO = new PackDAO();
    private StatusDAO statusDAO = new StatusDAO();

    @WebMethod
    public boolean addPack(Pack pack) {
        if (packDAO.findById(pack.getId()) != null || !packDAO.existentUser(pack.getId_client1()) || !packDAO.existentUser(pack.getId_client2())) {
            System.out.println("Attempting to add already existing pack or user does not exist for this operation.");
            return false;
        }
        packDAO.add(pack);
        return true;
    }

    @WebMethod
    public boolean deletePack(int id) {
        Pack p =packDAO.findById(id);
        if (p == null) {
            System.out.println("Attempting to delete inexisting pack");
            return false;
        }
        packDAO.delete(id);
        statusDAO.delete(id);
        return true;
    }

    @WebMethod
    public boolean registerPackForTracking(int pack_id) {
        Pack p = packDAO.findById(pack_id);
        if (p == null) {
            System.out.println("Attempting to register inexisting pack");
            return false;
        }
        if (p.isTracking() == true) {
            System.out.println("Pack already tracked");
            return false;
        }
        packDAO.updateTracking(pack_id);
        statusDAO.add(pack_id, p.getTo_city(), LocalDateTime.now());
        return true;
    }

    @WebMethod
    public boolean updatePackStatus(int pack_id, String city) {
        if (packDAO.findById(pack_id) == null) {
            System.out.println("Attempting to update inexisting pack");
            return false;
        }
        statusDAO.add(pack_id, city, LocalDateTime.now());
        return true;
    }

    @WebMethod
    public List<Status> findRoute(int pack_id) {
        return statusDAO.findAll(pack_id);
    }
}

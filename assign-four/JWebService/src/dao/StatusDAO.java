package dao;

import entity.Pack;
import entity.Status;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StatusDAO {

    public boolean add(int pack_id,String city, LocalDateTime ldt) {
        String query = "INSERT INTO Status (city, time, id_pack) VALUES (?,?,?)";
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(ldt.getYear(), ldt.getMonthValue()-1, ldt.getDayOfMonth(), ldt.getHour(), ldt.getMinute(), ldt.getSecond());
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, city);
            statement.setTimestamp(2, new Timestamp(calendar.getTimeInMillis()));
            statement.setInt(3, pack_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        System.out.println("status add success");
        return true;
    }

    public boolean delete(int pack_id) {
        String query = "DELETE FROM Status WHERE id=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, pack_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return  false;
        }
        System.out.println("status delete success");
        return true;
    }

    public List<Status> findAll(int pack_id) {
        String query = "SELECT * FROM Status WHERE id_pack=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, pack_id);
            ResultSet resultSet = statement.executeQuery();
            List<Status> route = new ArrayList<>();
            while (resultSet.next()) {
                Status s = new Status();
                s.setId((Integer)resultSet.getObject("id"));
                s.setCity((String)resultSet.getObject("city"));
                s.setTime(((Timestamp) resultSet.getObject("time")).toLocalDateTime());
                s.setId_pack(pack_id);
                route.add(s);
            }
            return route;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}

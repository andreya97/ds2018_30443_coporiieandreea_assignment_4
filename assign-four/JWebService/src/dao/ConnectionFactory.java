package dao;

import java.sql.*;

public class ConnectionFactory {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/assign-four-db?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "mysql";

    private static ConnectionFactory instance = new ConnectionFactory();

    private ConnectionFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DBURL, USER, PASS);
        } catch (SQLException e) {
            System.out.println( "Can't connect ot the db" + e.getMessage());
        }
        return connection;
    }

    public static Connection getConnection() {
        return instance.createConnection();
    }

    public static void close(Connection connection) throws SQLException {
        if (connection != null) connection.close();
    }

    public static void close(Statement statement) throws SQLException {
        if (statement != null) statement.close();

    }

    public static void close( ResultSet resultSet) throws SQLException {
        if (resultSet != null)  resultSet.close();
    }

}

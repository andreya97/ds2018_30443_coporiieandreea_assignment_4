package dao;
import entity.Pack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PackDAO {

    public PackDAO() {}

    public boolean add(Pack pack) {
        String query = "INSERT INTO Pack (id_client1, id_client2, name, description, from_city, to_city, tracking) VALUES (?,?,?,?,?,?,?)";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, pack.getId_client1());
            statement.setInt(2, pack.getId_client2());
            statement.setString(3, pack.getName());
            statement.setString(4, pack.getDescription());
            statement.setString(5, pack.getFrom_city());
            statement.setString(6, pack.getTo_city());
            statement.setBoolean(7, false);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean delete(int id) {
        String query = "DELETE FROM Pack WHERE id=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
           System.out.println(e.getMessage());
           return false;
        }
        return true;
    }

    public boolean updateTracking(int id) {
        String query = "UPDATE Pack SET tracking=? WHERE id=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setBoolean(1, true);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Pack findById(int id) {
        String query = "SELECT * FROM Pack WHERE id=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Pack p = new Pack();
                p.setId(id);
                p.setId_client1((Integer)resultSet.getObject("id_client1"));
                p.setId_client2((Integer)resultSet.getObject("id_client2"));
                p.setName((String)resultSet.getObject("name"));
                p.setDescription((String)resultSet.getObject("description"));
                p.setFrom_city((String)resultSet.getObject("from_city"));
                p.setTo_city((String)resultSet.getObject("to_city"));
                p.setTracking((Integer)resultSet.getObject("tracking") == 1 ? true : false);
                return p;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public boolean existentUser(int id) {
        String query = "SELECT * FROM User WHERE id=?";
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
